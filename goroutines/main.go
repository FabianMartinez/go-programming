package main

import (
	"fmt"
	"runtime"
	"sync"
)

func main() {
	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de GoRoutines: %v\n", runtime.NumGoroutine())

	var wg sync.WaitGroup
	wg.Add(2)

	go func() {
		fmt.Println("Hola desde primera goroutine")
		wg.Done()
	}()

	go func() {
		fmt.Println("Hola desde segunda goroutine")
		wg.Done()
	}()

	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de GoRoutines: %v\n", runtime.NumGoroutine())

	wg.Wait()

	fmt.Println("A punto de finalizar")

	fmt.Printf("Numero de CPUs: %v\n", runtime.NumCPU())
	fmt.Printf("Numero de GoRoutines: %v\n", runtime.NumGoroutine())
}
