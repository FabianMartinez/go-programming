package main

import "fmt"

func main() {
	c := make(chan int)

	//enviando
	go func() {
		for i := 0; i <= 5; i++ {
			c <- i
		}
		close(c)
	}()
	//recibiendo
	for v := range c {
		fmt.Println(v)
	}

	fmt.Println("Programa finalizado")
}
