package main

import "fmt"

func main() {
	C := make(chan int)

	//sending
	go channelSending(C)
	//receiving
	channelReceive(C)

	fmt.Println("Finalizando...")
}

func channelSending(cSending chan<- int) {
	cSending <- 5000
}

func channelReceive(cReceive <-chan int) {
	fmt.Println(<-cReceive)
}
