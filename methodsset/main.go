package main

import "fmt"

type persona struct {
	nombre string
	edad   int
}

type humano interface {
	hablar()
}

func (p *persona) hablar() {
	fmt.Println("Hola, soy", p.nombre)
}

func diAlgo(h humano) {
	h.hablar()
}

func main() {
	p1 := persona{
		nombre: "Fabian",
		edad:   22,
	}

	diAlgo(&p1)
	p1.hablar()
}
