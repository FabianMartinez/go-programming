package main

import (
	"fmt"

	"gitlab.com/FabianMartinez/go-programming/packages/cat"
)

func main() {
	fmt.Println("Hello from dog!")

	cat.Hola()
	cat.Eat()
}
